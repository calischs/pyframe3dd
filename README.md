Some hacked utilities for interfacing to the Frame3DD library, maintained by Henri P. Gavin, Department of Civil and Environmental Engineering, Duke University.
frame3dd.sourceforge.net




### Installation

1. Install <a href='http://frame3dd.sourceforge.net/'>Frame3dd</a>.
2. Install python dependencies
```
sudo pip install networkx scipy
```
3. Build and install pyframe3dd.  By default this will go to /usr/local
```
python setup.py build
python setup.py install
```
4. (optional) If you want to see Frame3dd's native output plots and animations, install gnuplot.
On mac:
```
brew install gnuplot --with-x11
```
or on Linux
```
sudo apt-get install gnuplot
```

### Examples

The directory <a herf='ex'>ex/</a> contains example python scripts using the library.
